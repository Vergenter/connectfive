/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Board;

import java.util.HashMap;
/**
 *
 * @author Ja
 */
public class BoardModel{
    private int xCord;
    private int yCord;
    HashMap<Key,ChunkModel> board;
    
    public BoardModel(int x, int y){
        xCord=x;
        yCord=y;
        board=new HashMap<Key,ChunkModel>();
    }
    
    public boolean doesChunkExist(int x,int y){
        return board.get(new Key(x,y))!=null;
    }
    
    public ChunkModel getChunk(int x, int y){
        ChunkModel temp=board.get(new Key(x,y));
        return temp == null ? new ChunkModel(x,y,this) : temp;
    }
    
    public void addChunk(int x, int y, ChunkModel newChunk) {
        board.put(new Key(x,y),newChunk);
    }
    
    

}
