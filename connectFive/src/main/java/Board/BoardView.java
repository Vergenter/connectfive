/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Board;
import Observer.Observer;
import Observer.Subject;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JViewport;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;

/**
 *
 * @author Ja
 */

public class BoardView extends JPanel{
    BoardModel myData;
    Subject buttonSubject;
    public void addButtonClickedObserver(Observer toAdd){
        buttonSubject.attach(toAdd);
        reMakeMap();
    }
    public void moveCamera(int xUpperLeftVisibleCorner,int yUpperLeftVisibleCorner){
        //1 is a buffer to map let player scroll map
        VievX=xUpperLeftVisibleCorner-2;
        VievY=yUpperLeftVisibleCorner-2;
        reMakeMap();
        Rectangle view=map.getVisibleRect();
        Dimension ChunkPixelSize = map.getComponent(0).getSize();
        view.x =ChunkPixelSize.width*1+1;
        view.y =ChunkPixelSize.height*1+1;
        map.scrollRectToVisible(view);
    }
    
    int VievLenghtX;
    int VievLenghtY;
    int VievX;
    int VievY;
    private JPanel map;
    int boardPadding=25;
    ChunkView[][] tab;
    BoardController controller;
    public void changeButtonColor(int xboard,int yboard,int xchunk,int ychunk,Color color){
        if(xboard>=VievX&&xboard<=VievX+VievLenghtX&&yboard>=VievY&&yboard<=VievY+VievLenghtY)
            tab[xboard-VievX][yboard-VievY].changeButtonColor(xchunk, ychunk, color);
    }
    public BoardView(BoardModel data,int x,int y,int viewx,int viewy) {
        super(new BorderLayout());
        buttonSubject=new Subject();
        myData=data;
        VievLenghtX=viewx;
        VievLenghtY=viewy;
        tab= new ChunkView[VievLenghtX][VievLenghtY];
        VievX=x;
        VievY=y;
        makeMap();
    }
    
    public void addListener(BoardController controller) {
        this.controller = controller;
    }
    private void reMakeMap(){
        remove(getComponent(0));
        makeMap();
    }
    private void makeMap(){
        map=new JPanel();
        
        //map.setBorder(new EmptyBorder(boardPadding, boardPadding, boardPadding, boardPadding));
        
        map.setLayout(new GridBagLayout());
        GridBagConstraints gbc=new GridBagConstraints();
        
        for (int i=VievX;i<VievX+VievLenghtX;i++){
            for(int j=VievY;j<VievY+VievLenghtY;j++){
                    gbc.gridx=i-VievX;
                    gbc.gridy=j-VievY;  
                    tab[i-VievX][j-VievY]=new ChunkView(myData.getChunk(i, j),map,buttonSubject);
                    map.add(tab[i-VievX][j-VievY],gbc);
            }
        }
        
        //map.setPreferredSize(new Dimension(200,200));
        //map.setAutoscrolls(true);
        JScrollPane ab=new JScrollPane(map, JScrollPane.VERTICAL_SCROLLBAR_NEVER,JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
       //JScrollPane js = new JScrollPane(map, JScrollPane.VERTICAL_SCROLLBAR_NEVER,JScrollPane.HORIZONTAL_SCROLLBAR_NEVER));
        ab.setPreferredSize(new Dimension(200,200));
        
        add(ab);
        
        MouseAdapter ma = new MouseAdapter() {

                    private Point origin;
                    int deltaX;
                    int deltaY;
                    @Override
                    public void mousePressed(MouseEvent e) {
                        origin = new Point(e.getPoint());
                    }

                    @Override
                    public void mouseReleased(MouseEvent e) {
                        //alter map
                        int deltaX=0;
                        int deltaY=0;
                        Dimension ChunkPixelSize = map.getComponent(0).getSize();
                        Rectangle view = map.getVisibleRect();
                        //System.out.println("1:"+view.x+" "+view.y);
                        
                        if(view.x<ChunkPixelSize.width)
                            --deltaX;
                        if(view.y<ChunkPixelSize.height)
                            --deltaY;
                        if(view.x+view.width>ChunkPixelSize.width*(VievLenghtX-1))
                            ++deltaX;
                        if(view.y+view.height>ChunkPixelSize.height*(VievLenghtY-1))
                            ++deltaY;
                        VievX+=deltaX;
                        VievY+=deltaY;
                        view.x -=ChunkPixelSize.width*deltaX;
                        view.y -=ChunkPixelSize.height*deltaY;

                        reMakeMap();
                        map.scrollRectToVisible(view);
                        ChunkModel my= myData.getChunk(0, 0);
                        //for (int i=0;i<my.getchunkSize();i++){
                        //        for (int j=0;j<my.getchunkSize();j++)
                        //            System.out.print(""+my.getState(i, j));
                        //        System.out.println("");
                        //}
                        //System.out.println("3:"+VievX+" "+VievY);
                        //System.out.println("4:"+ChunkPixelSize.width+" "+ChunkPixelSize.height);
                    }

                    @Override
                    public void mouseDragged(MouseEvent e) {
                        if (origin != null) {
                            JViewport viewPort = (JViewport) SwingUtilities.getAncestorOfClass(JViewport.class, map);
                            if (viewPort != null) {
                                deltaX = (origin.x - e.getX());
                                deltaY = (origin.y - e.getY());

                                Rectangle view = viewPort.getViewRect();
                                view.x += deltaX;
                                view.y += deltaY;
                                map.scrollRectToVisible(view);
                                //System.out.println(""+view.x+" "+view.y);
                            }
                        }
                    }

                };

                map.addMouseListener(ma);
                map.addMouseMotionListener(ma);
                
        
        SwingUtilities.updateComponentTreeUI(this);
    }
}

