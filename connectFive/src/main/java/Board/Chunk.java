/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Board;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JViewport;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;
import Observer.Subject;
import CurrentPlayerSingleton.CurrentPlayerSingleton;

/**
 *
 * @author Ja
 */
class ChunkModel{
    
    private BoardModel BoardParent;
    public BoardModel getBoardParent() {
        return BoardParent;
    }
    
    public int getWart(int x,int y){
        int dx=0,dy=0;
        if(x<0)
            dx--;
        if(y<0)
            dy--;
        if(x>=chunkSize)
            dx++;
        if(y>=chunkSize)
            dy++;
        if(dx==0 && dy==0)
            return getState(x,y);
        else{
            if(BoardParent.doesChunkExist(xCord+dx,yCord+dy))
                return BoardParent.getChunk(xCord+dx,yCord+dy).getWart(x-dx*chunkSize, y-dy*chunkSize);
            else
                return 0;     
       }
       
    }

    private int xCord;
    public int getxCord(){
        return xCord;
    }
    
    private int yCord;
    public int getyCord(){
        return yCord;
    }
    
    private static int chunkSize = 4;
    public int getChunkSize(){
        return chunkSize;
    }
    
    private char[][] stateArray; //Enum 3 state none circle cross
    public char getState(int x, int y){
        return stateArray[x][y];
    }
    
    
    public void setState(int x, int y, char newState){
        stateArray[x][y] = newState;
        // tell parent
        // BoardParent.notify(myx,myy,setx,sety)
    }   
    
    public void saveChunk(){
        BoardParent.addChunk(xCord,yCord,this);
    }
            
    public ChunkModel(int x,int y,BoardModel Parent){
        BoardParent=Parent;
        xCord=x;
        yCord=y;
        stateArray= new char[chunkSize][chunkSize];
      
    }
}



class ChunkView extends JPanel {
    //private int xCord;
   // private int yCord;
    JPanel ForMouse;
    static int buttonSize = 43;
    private ChunkModel myData;
    private JButton[][] buttonArray;
    public void changeButtonColor(int x,int y,Color color){
        buttonArray[x][y].setBackground(color);
    }
    private Subject myButtonPressed;
    public ChunkView(ChunkModel data, JPanel forMouse,Subject buttonPressed){
        myButtonPressed=buttonPressed;
        CurrentPlayerSingleton currentPlayer = CurrentPlayerSingleton.getInstance();
        ForMouse=forMouse;
        setBorder(new EmptyBorder(2, 2, 2, 2));
        setBackground(Color.BLACK);
        myData=data;
        int chunkSize=myData.getChunkSize();
        buttonArray = new JButton[chunkSize][chunkSize];
        setLayout(new GridLayout(chunkSize,chunkSize,4,4));
        for(int j = 0; j < chunkSize; j++) {
        for(int i = 0; i < chunkSize; i++) {
            
                buttonArray[i][j] = new JButton(String.valueOf(myData.getState(i,j)));
                
                MouseAdapter ma = new MouseAdapter()                
                {
                    @Override
                    public void mousePressed(MouseEvent e) {
                        if(SwingUtilities.isRightMouseButton(e))
                            ForMouse.getMouseListeners()[0].mousePressed(e);
                    }

                    @Override
                    public void mouseReleased(MouseEvent e) {
                        if(SwingUtilities.isRightMouseButton(e))
                            ForMouse.getMouseListeners()[0].mouseReleased(e);
                    }

                    @Override
                    public void mouseDragged(MouseEvent e) {   
                        if(SwingUtilities.isRightMouseButton(e))
                            ForMouse.getMouseMotionListeners()[0].mouseDragged(e);
                    }
                };
                
                final int x = i;
                final int y = j;
                if(myData.getState(i, j) == 'O') {
                    buttonArray[i][j].setBackground(currentPlayer.getColorO());
                } else if (myData.getState(i, j) == 'X'){
                    buttonArray[i][j].setBackground(currentPlayer.getColorX());
                } else { 
                    buttonArray[i][j].setBackground(Color.WHITE);
                }
                buttonArray[i][j].addMouseListener(ma);
                buttonArray[i][j].addMouseMotionListener(ma);
                if(!currentPlayer.getGameFinishedState())
                buttonArray[i][j].addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        if(!currentPlayer.getGameFinishedState())
                        if(myData.getState(x, y) == 0) {
                            myData.setState(x, y, currentPlayer.getPlayerSymbol());
                            myData.saveChunk();
                            //buttonArray[x][y].setText(String.valueOf(myData.getState(x, y)));
                            buttonArray[x][y].setText(String.valueOf(myData.getState(x, y)));
                            //checkWin(x, y, myData);
                            buttonArray[x][y].setBackground(currentPlayer.getPlayerColor());
                            currentPlayer.setLastMove(myData.getxCord(), myData.getyCord(), x, y);
                            myButtonPressed.myNotify();
                            currentPlayer.changePlayer();
                            
                        }
                    }
                });
                buttonArray[i][j].setPreferredSize(new Dimension(buttonSize, buttonSize));
                add(buttonArray[i][j]);// constraints);
                
            }
        }
        
    }
}
