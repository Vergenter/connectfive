/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MyOverlayLayout;

import java.awt.Component;
import MyWindow.MyWindow;
import Board.BoardModel;
import Board.BoardView;
import Board.BoardController;
import Menu.Menu;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.OverlayLayout;
import javax.swing.border.LineBorder;
import CurrentPlayerSingleton.CurrentPlayerSingleton;
import MyOverlayLayout.TransparentPanel.TransparentPanelControler;
import MyOverlayLayout.TransparentPanel.TransparentPanelModel;
import MyOverlayLayout.TransparentPanel.TransparentPanelView;
/**
 *
 * @author Takedown
 */
public class MyOverlayLayout {
    private BoardController bc;
    private MyWindow mainWindow;
    private JButton menuButton = new JButton("Menu");
    private JButton restartButton = new JButton("Nowa gra");
    private JPanel winPanel;
    JPanel boardPanel;
    public MyOverlayLayout() {
        mainWindow = createMainWindow();
        mainWindow.setVisible(true);
    }
    
    private MyWindow createMainWindow() {
        boardPanel = createMainPanel();
        this.restartButton = createRestartButton();
        Menu menuView = new Menu();
        MyWindow frame = new MyWindow("ConnectFive", boardPanel, menuView, this.restartButton);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(new Dimension(1024, 768));
        menuButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frame.getMyWindowFacade().openMenu();
            }
        });
        return frame;
    }
    
    public JButton createRestartButton() {
        restartButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
            CurrentPlayerSingleton.getInstance().restartCurrentPlayerSingleton();
            mainWindow.restartBoard(createMainPanel());
            }
        });
        restartButton.setFont(new Font("Arial", Font.PLAIN, 32));
        return restartButton;
    }
    
    private JPanel createMainPanel() {
        JPanel mainPanel = new JPanel() {
            @Override
            public boolean isOptimizedDrawingEnabled() {
                return false;
            }
        };
        
        mainPanel.setLayout(new OverlayLayout(mainPanel));
        
        BoardModel bm = new BoardModel(0, 0);
        BoardView bv = new BoardView(bm, 0, 0, 8, 8);
        bc=new BoardController(bm,bv);
        
        JPanel transparentPanel = createTransparentPanel(bc);//needs boardcontroler
        mainPanel.add(transparentPanel);
        
        this.winPanel = createWinPanel();
        mainPanel.add(this.winPanel).setVisible(true);
        mainPanel.add(bv);
        
        return mainPanel;
    }
    
    private JPanel createTransparentPanel(BoardController bc) {
        TransparentPanelModel data= new TransparentPanelModel();
        TransparentPanelView t =new TransparentPanelView(menuButton,data);
        TransparentPanelControler TPC;
        TPC = new TransparentPanelControler(data,t,bc);
        return t;
        
       
    }

    public JPanel createWinPanel(){
        
        winPanel = new JPanel();
        winPanel.setPreferredSize(new Dimension(260, 100));
        winPanel.setOpaque(false);
        winPanel.setBorder(new LineBorder(Color.BLACK));
        winPanel.setVisible(true);
        CurrentPlayerSingleton cps = CurrentPlayerSingleton.getInstance();
        
        JPanel tmpPanel = new JPanel();
        tmpPanel.setBackground(new Color(115, 215, 135, 240));
        String text;
        if(cps.getGameFinishedState()) {
            text = "Wygrana gracza " + cps.getTheOtherPlayerSymbol();
        } else {
            text = "Tura gracza " + cps.getPlayerSymbol();
        }
        JLabel label = new JLabel(text);
        label.setFont(new Font("Arial", Font.PLAIN, 28));
        tmpPanel.add(label);
        winPanel.add(tmpPanel);
        winPanel.updateUI();  
        return winPanel;
    }
    
    public JPanel getWinPanel() {
        return this.winPanel;
    }
    
    public void updateWinPanel() {
        CurrentPlayerSingleton cps = CurrentPlayerSingleton.getInstance();
        String text;
        if(cps.getGameFinishedState()) {
            text = "Wygrana gracza " + cps.getTheOtherPlayerSymbol();
        } else {
            text = "Tura gracza " + cps.getPlayerSymbol();
        }
        JLabel label = new JLabel(text);
        label.setFont(new Font("Arial", Font.PLAIN, 28));
        this.winPanel.removeAll();
        
        
        JPanel tmpPanel = new JPanel();
        tmpPanel.setBackground(new Color(115, 215, 135, 240));
        tmpPanel.add(label);
        this.winPanel.add(tmpPanel);
        this.winPanel.updateUI(); 
        this.winPanel.setVisible(true);
    }

    
}
