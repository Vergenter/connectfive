/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MyOverlayLayout.TransparentPanel;

import Board.BoardController;
import Observer.Observer;

/**
 *
 * @author Ja
 */
public class TransparentPanelControler {
    BoardController comuniacation;
    Observer myButtonClickObserver;
    TransparentPanelModel model;
    TransparentPanelView view;
    public TransparentPanelControler(TransparentPanelModel addModel, TransparentPanelView addView, BoardController communicationWithBoardControler){
        model=addModel;
        view=addView;
        comuniacation=communicationWithBoardControler;
         myButtonClickObserver= new Observer(){
                @Override 
                public void update(){
                    model.addMove();
                    view.update();
                }
        };
        comuniacation.addButtonClickedObserverToBoard(myButtonClickObserver);
        view.addControler(this);
    }
    public void askBoardToMoveCamera(int boardx,int boardy,int chunkx,int chunky){
         comuniacation.moveCamera(boardx,boardy);
         comuniacation.DistinctButton(boardx,boardy,chunkx,chunky);
    }
    public Observer getObserverForBoard(){
        return myButtonClickObserver;
    }
}
