/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MyWindow;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import static javax.swing.WindowConstants.EXIT_ON_CLOSE;
import javax.swing.JPanel;
import Menu.Menu;
import MyOverlayLayout.MyOverlayLayout;
import MyWindowFacade.MyWindowFacade;
/**
 *
 * @author Takedown
 */
public class MyWindow extends JFrame{
    int number;
    
    private JButton startGameButton = new JButton("Kontynuuj");
    private JPanel boardPanel;
    private Menu menuView;
    private MyWindowFacade myWindowFacede;
    private JButton restartButton;
    public MyWindow(String title, JPanel newBoardPanel, Menu newMenuView, JButton restartButton){
        super(title);
        boardPanel = newBoardPanel;
        menuView = newMenuView;
        this.restartButton = restartButton;
        menuView.createButtons(createStartGameButton(), restartButton);
        myWindowFacede = new MyWindowFacade(this);
        setSize(1024, 768);
        setResizable(false);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        
        myWindowFacede.startGame();
    }
    
    public JButton createStartGameButton() {
        startGameButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                myWindowFacede.startGame();
            }
        });
        startGameButton.setFont(new Font("Arial", Font.PLAIN, 32));
        return startGameButton;
    }
    
    public void restartBoard(JPanel newBoardPanel) {
        boardPanel = newBoardPanel;
        menuView.createButtons(createStartGameButton(), this.restartButton);
        myWindowFacede.startGame();
    }
    
    
    public MyWindowFacade getMyWindowFacade() {
        return this.myWindowFacede;
    }
    
    public JPanel getBoardPanel() {
        return this.boardPanel;
    }
    
    public Menu getMenuView() {
        return this.menuView;
    }
}

