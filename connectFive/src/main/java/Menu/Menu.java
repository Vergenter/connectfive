/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Menu;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import MyOverlayLayout.MyOverlayLayout;
import MyWindow.MyWindow;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import static javax.swing.JFrame.EXIT_ON_CLOSE;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import CurrentPlayerSingleton.CurrentPlayerSingleton;

public class Menu extends JPanel {

    private JButton startButton;
    private JButton exitButton = new JButton("Wyjdź");
    private JButton restartButton;
    JPanel panel;
    
    
    public Menu() {
        //super("Menu");
        //setDefaultCloseOperation(EXIT_ON_CLOSE);
        panel = new JPanel(new GridBagLayout());
        //pack();
        //setLocationRelativeTo(null);
        setSize(1024, 768);
    }
    
    public void createButtons(JButton newStartButton, JButton newRestartButton) {
        GridBagConstraints constraints = new GridBagConstraints();
        
        //start continue
        startButton = newStartButton;
        constraints.anchor = GridBagConstraints.WEST;
        constraints.insets = new Insets(10, 100, 10, 100);
        constraints.gridx = 0;
        constraints.gridy = 0;
        constraints.gridwidth = 3;
        startButton.setPreferredSize(new Dimension(300, 100));
        panel.add(startButton, constraints);
        
        //restart
        restartButton = newRestartButton;
        constraints.anchor = GridBagConstraints.WEST;
        constraints.insets = new Insets(10, 100, 10, 100);
        constraints.gridx = 0;
        constraints.gridy = 1;
        constraints.gridwidth = 3;
        restartButton.setPreferredSize(new Dimension(300, 100));
        panel.add(restartButton, constraints);
        
        //exit
        exitButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });
        
        constraints.anchor = GridBagConstraints.WEST;
        constraints.gridx = 0;
        constraints.gridy = 2;
        constraints.gridwidth = 3;
        exitButton.setPreferredSize(new Dimension(300, 100));
        exitButton.setFont(new Font("Arial", Font.PLAIN, 32));
        panel.add(exitButton, constraints);
        
        add(panel);
    }
}
